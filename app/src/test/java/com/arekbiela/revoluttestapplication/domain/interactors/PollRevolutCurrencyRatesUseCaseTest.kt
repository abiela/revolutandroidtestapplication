package com.arekbiela.revoluttestapplication.domain.interactors

import android.accounts.NetworkErrorException
import com.arekbiela.revoluttestapplication.domain.policy.ExponentialRxPollRetryPolicy
import com.arekbiela.revoluttestapplication.model.data.CurrencyRates
import com.arekbiela.revoluttestapplication.model.exception.HttpCallFailureRequestException
import com.arekbiela.revoluttestapplication.model.exception.MaxNetworkRequestRetriesExceeded
import com.arekbiela.revoluttestapplication.model.repository.CurrencyRatesRepository
import com.arekbiela.revoluttestapplication.model.exception.NetworkTimeoutRequestException
import io.reactivex.Single
import io.reactivex.observers.BaseTestConsumer
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Test
import org.mockito.Mockito
import retrofit2.HttpException
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*
import java.util.concurrent.TimeUnit

class PollRevolutCurrencyRatesUseCaseTest {

    val repository: CurrencyRatesRepository = mock<CurrencyRatesRepository>()

    val pollingIntervalAmount: Long = 1L
    val pollingIntervalUnit: TimeUnit = TimeUnit.SECONDS
    val rxPollRetryPolicy = ExponentialRxPollRetryPolicy(3)

    val pollRevolutCurrencyRatesUseCase =
        PollRevolutCurrencyRatesUseCase(
            repository,
            pollingIntervalAmount,
            pollingIntervalUnit,
            rxPollRetryPolicy
        )

    @Test
    fun returnsCurrencyRatesCorrectlyWhenRepositoryReturnsValues() {
        //given
        val baseCurrency = Currency.getInstance("EUR")
        val firstCurrencyRatesUpdate = CurrencyRates(
            baseCurrency = baseCurrency,
            ratesMap = mapOf(Pair(Currency.getInstance("USD"), 1.12f))
        )
        val secondCurrencyRatesUpdate = CurrencyRates(
            baseCurrency = baseCurrency,
            ratesMap = mapOf(Pair(Currency.getInstance("USD"), 1.132f))
        )

        //when
        var pollRequests = 0
        Mockito.`when`(repository.getSingleCurrencyRatesForBase(baseCurrency))
            .thenReturn(Single.fromCallable {
                pollRequests++
                when (pollRequests) {
                    1 -> firstCurrencyRatesUpdate
                    else -> secondCurrencyRatesUpdate
                }
            })

        //then
        pollRevolutCurrencyRatesUseCase
            .execute(baseCurrency)
            .test()
            .assertNoErrors()
            .awaitCount(3)
            .assertValueAt(0, firstCurrencyRatesUpdate)
            .assertValueAt(1, secondCurrencyRatesUpdate)
            .assertValueAt(2, secondCurrencyRatesUpdate)
    }

    @Test
    fun retriesAfterHttpExceptionAndReturnsSuccessfullyWithinRangeOfRetryPolicy() {
        //given
        val baseCurrency = Currency.getInstance("EUR")
        val firstCurrencyRatesUpdate = CurrencyRates(
            baseCurrency = baseCurrency,
            ratesMap = mapOf(Pair(Currency.getInstance("USD"), 1.12f))
        )

        //when=
        var pollRequests = 0
        Mockito.`when`(repository.getSingleCurrencyRatesForBase(baseCurrency))
            .thenReturn(Single.fromCallable {
                pollRequests++
                when (pollRequests) {
                    1,2 -> throw HttpCallFailureRequestException("Test message")
                    else -> firstCurrencyRatesUpdate
                }
            })

        //then
        pollRevolutCurrencyRatesUseCase
            .execute(baseCurrency)
            .test()
            .awaitCount(1, BaseTestConsumer.TestWaitStrategy.SLEEP_100MS, 15000)
            .assertValueAt(0, firstCurrencyRatesUpdate)
    }

    @Test
    fun returnsErrorAfterExceedingRetryPolicyMaxCount() {
        //given
        val baseCurrency = Currency.getInstance("EUR")
        val firstCurrencyRatesUpdate = CurrencyRates(
            baseCurrency = baseCurrency,
            ratesMap = mapOf(Pair(Currency.getInstance("USD"), 1.12f))
        )

        //when
        var pollRequests = 0
        Mockito.`when`(repository.getSingleCurrencyRatesForBase(baseCurrency))
            .thenReturn(Single.fromCallable {
                pollRequests++
                when (pollRequests) {
                    1,2,3,4 -> throw HttpCallFailureRequestException("Error message")
                    else -> firstCurrencyRatesUpdate
                }
            })

        //then
        pollRevolutCurrencyRatesUseCase
            .execute(baseCurrency)
            .test()
            .await()
            .assertFailure(MaxNetworkRequestRetriesExceeded::class.java)
    }

    @Test
    fun returnsErrorImmediatelyWhenNetworkTimeoutExceptionOccurs() {
        //given
        val baseCurrency = Currency.getInstance("EUR")

        //when
        Mockito.`when`(repository.getSingleCurrencyRatesForBase(baseCurrency))
            .thenReturn(Single.error(NetworkTimeoutRequestException("Test message")))

        //then
        pollRevolutCurrencyRatesUseCase
            .execute(baseCurrency)
            .test()
            .await()
            .assertFailure(NetworkTimeoutRequestException::class.java)
    }

    inline fun <reified T : Any> mock(): T = Mockito.mock(T::class.java)
}