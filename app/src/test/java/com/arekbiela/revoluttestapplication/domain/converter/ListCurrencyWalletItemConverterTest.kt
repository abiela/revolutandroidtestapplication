package com.arekbiela.revoluttestapplication.domain.converter

import com.arekbiela.revoluttestapplication.presentation.model.CurrencyWallet
import com.arekbiela.revoluttestapplication.presentation.model.CurrencyWalletItem
import org.junit.Test

import org.junit.Assert.*
import java.util.*

class ListCurrencyWalletItemConverterTest {

    val listCurrencyWalletItemConverter = ListCurrencyWalletItemConverter()

    @Test
    fun convertsCurrencyWalletIntoItemList() {
        //given
        val currencyAmountMap = mapOf(Pair(Currency.getInstance("USD"), 100.0f), Pair(Currency.getInstance("EUR"), 110.0f))
        val currencyWallet = CurrencyWallet(currencyAmountMap)

        val usdCurrencyItemList = CurrencyWalletItem(Currency.getInstance("USD"), 100.0f)
        val eurCurrencyItemList = CurrencyWalletItem(Currency.getInstance("EUR"), 110.0f)

        //when
        val list = listCurrencyWalletItemConverter.convertCurrencyWalletIntoItems(currencyWallet)

        //then
        assertTrue(list.size == 2)
        assertTrue(list.contains(usdCurrencyItemList))
        assertTrue(list.contains(eurCurrencyItemList))
    }

    @Test
    fun returnsEmptyListWhenCurrencyWalletIsEmpty() {
        //given
        val currencyWallet = CurrencyWallet(emptyMap())

        //when
        val list = listCurrencyWalletItemConverter.convertCurrencyWalletIntoItems(currencyWallet)

        //then
        assertTrue(list.isEmpty())
    }
}