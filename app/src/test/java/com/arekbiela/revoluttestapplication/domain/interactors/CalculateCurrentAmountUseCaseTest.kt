package com.arekbiela.revoluttestapplication.domain.interactors

import com.arekbiela.revoluttestapplication.domain.calculator.CurrencyWalletCalculator
import com.arekbiela.revoluttestapplication.domain.converter.ListCurrencyWalletItemConverter
import com.arekbiela.revoluttestapplication.domain.policy.ExponentialRxPollRetryPolicy
import com.arekbiela.revoluttestapplication.model.data.CurrencyRates
import com.arekbiela.revoluttestapplication.model.repository.CurrencyRatesRepository
import com.arekbiela.revoluttestapplication.presentation.model.CurrencyWalletItem
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito
import java.util.*
import java.util.concurrent.TimeUnit

class CalculateCurrentAmountUseCaseTest {

    private val repository: CurrencyRatesRepository = Mockito.mock(CurrencyRatesRepository::class.java)

    private val pollingIntervalAmount: Long = 1L
    private val pollingIntervalUnit: TimeUnit = TimeUnit.SECONDS
    private val rxPollRetryPolicy = ExponentialRxPollRetryPolicy(3)

    private val pollRevolutCurrencyRatesUseCase =
        PollRevolutCurrencyRatesUseCase(repository, pollingIntervalAmount, pollingIntervalUnit, rxPollRetryPolicy)

    private val currencyWalletCalculator = CurrencyWalletCalculator()
    private val currencyWalletItemConverter = ListCurrencyWalletItemConverter()

    private val calculateCurrentAmountUseCase = CalculateCurrentAmountUseCase(pollRevolutCurrencyRatesUseCase, currencyWalletCalculator, currencyWalletItemConverter)

    @Test
    fun returnsCorrectCurrencyListItemWhenRepositoryAnswersCorrectly() {
        //given
        val baseCurrency = Currency.getInstance("EUR")
        val baseCurrencyAmount = 100.0f
        val firstCurrencyRatesUpdate = CurrencyRates(
            baseCurrency = baseCurrency,
            ratesMap = mapOf(Pair(Currency.getInstance("USD"), 1.12f))
        )
        val secondCurrencyRatesUpdate = CurrencyRates(
            baseCurrency = baseCurrency,
            ratesMap = mapOf(Pair(Currency.getInstance("USD"), 1.132f))
        )

        val firstCurrencyWalletItem = listOf(
            CurrencyWalletItem(Currency.getInstance("EUR"), 100.0f),
            CurrencyWalletItem(Currency.getInstance("USD"), 112.0f))

        val secondCurrencyWalletItem = listOf(
            CurrencyWalletItem(Currency.getInstance("EUR"), 100.0f),
            CurrencyWalletItem(Currency.getInstance("USD"), 113.2f)
        )

        //when
        var pollRequests = 0
        Mockito.`when`(repository.getSingleCurrencyRatesForBase(baseCurrency))
            .thenReturn(Single.fromCallable {
                pollRequests++
                when (pollRequests) {
                    1 -> firstCurrencyRatesUpdate
                    else -> secondCurrencyRatesUpdate
                }
            })

        //then
        calculateCurrentAmountUseCase
            .execute(baseCurrency, baseCurrencyAmount)
            .test()
            .awaitCount(2)
            .assertValueAt(0, firstCurrencyWalletItem)
            .assertValueAt(1, secondCurrencyWalletItem)
    }

    @Test
    fun returnsCorrectCurrentWalletItemValuesAfterAmountChange() {
        //given
        val baseCurrency = Currency.getInstance("EUR")
        val baseCurrencyAmount = 100.0f
        val firstCurrencyRatesUpdate = CurrencyRates(
            baseCurrency = baseCurrency,
            ratesMap = mapOf(Pair(Currency.getInstance("USD"), 1.12f))
        )
        val secondCurrencyRatesUpdate = CurrencyRates(
            baseCurrency = baseCurrency,
            ratesMap = mapOf(Pair(Currency.getInstance("USD"), 1.132f))
        )

        val currencyWalletItem = listOf(
            CurrencyWalletItem(Currency.getInstance("EUR"), 200.0f),
            CurrencyWalletItem(Currency.getInstance("USD"), 226.4f)
        )

        //when
        var pollRequests = 0
        Mockito.`when`(repository.getSingleCurrencyRatesForBase(baseCurrency))
            .thenReturn(Single.fromCallable {
                pollRequests++
                when (pollRequests) {
                    1 -> firstCurrencyRatesUpdate
                    else -> secondCurrencyRatesUpdate
                }
            })

        //then
        val testObserver = calculateCurrentAmountUseCase
            .execute(baseCurrency, baseCurrencyAmount)
            .test()
        calculateCurrentAmountUseCase.changeAmount(200.0f)

        testObserver
            .awaitDone(3, TimeUnit.SECONDS)
            .assertValueCount(4)
            .assertValueAt(3, currencyWalletItem)
    }

    @Test
    fun returnsCorrectCurrentWalletItemValuesAfterBaseChange() {
        //given
        val baseCurrency = Currency.getInstance("EUR")
        val baseCurrencyAmount = 100.0f

        val newBaseCurrency = Currency.getInstance("USD")

        val firstCurrencyRatesUpdate = CurrencyRates(
            baseCurrency = baseCurrency,
            ratesMap = mapOf(Pair(Currency.getInstance("USD"), 1.12f))
        )
        val newCurrencyRatesUpdate = CurrencyRates(
            baseCurrency = newBaseCurrency,
            ratesMap = mapOf(Pair(Currency.getInstance("EUR"), 0.910f))
        )

        val currencyWalletItem = listOf(
            CurrencyWalletItem(Currency.getInstance("USD"), 100.0f),
            CurrencyWalletItem(Currency.getInstance("EUR"), 91.0f)
        )

        //when
        Mockito.`when`(repository.getSingleCurrencyRatesForBase(baseCurrency))
            .thenReturn(Single.just(firstCurrencyRatesUpdate))

        Mockito.`when`(repository.getSingleCurrencyRatesForBase(newBaseCurrency))
            .thenReturn(Single.just(newCurrencyRatesUpdate))

        //then
        val testObserver = calculateCurrentAmountUseCase
            .execute(baseCurrency, baseCurrencyAmount)
            .test()
        calculateCurrentAmountUseCase.changeBase(newBaseCurrency, 100.0f)

        testObserver
            .awaitDone(3, TimeUnit.SECONDS)
            .assertValueCount(3)
            .assertValueAt(2, currencyWalletItem)
    }
}