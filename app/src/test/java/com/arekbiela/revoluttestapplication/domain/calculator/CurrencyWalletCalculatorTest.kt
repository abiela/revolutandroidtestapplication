package com.arekbiela.revoluttestapplication.domain.calculator

import com.arekbiela.revoluttestapplication.model.data.CurrencyRates
import org.junit.Assert.*
import org.junit.Test
import java.util.*

class CurrencyWalletCalculatorTest {

    val currencyWalletCalculator = CurrencyWalletCalculator()

    @Test
    fun calculatesCurrencyWalletCorrectlyWhenGivenCurrencyRates() {
        //given
        val baseCurrencyAmount = 100.0f
        val currencyRates = CurrencyRates(ratesMap = mapOf(Pair(Currency.getInstance("USD"), 1.213f), Pair(Currency.getInstance("PLN"), 0.345f)))

        //when
        val currencyWalletMap = currencyWalletCalculator.calculate(baseCurrencyAmount, currencyRates).moneyAmountInCurrenciesMap

        //then
        assertTrue(currencyWalletMap.size == 3)
        assertTrue(currencyWalletMap.keys.contains(Currency.getInstance("USD")))
        assertTrue(currencyWalletMap.get(Currency.getInstance("USD")) == 121.3f)
        assertTrue(currencyWalletMap.keys.contains(Currency.getInstance("PLN")))
        assertTrue(currencyWalletMap.get(Currency.getInstance("PLN")) == 34.5f)
    }

    @Test
    fun returnsOnlySourceCurrencyWalletWhenEmptyCurrencyRatesGiven() {
        //given
        val baseCurrencyAmount = 100.0f
        val currencyRates = CurrencyRates()

        //when
        val currencyWalletMap = currencyWalletCalculator.calculate(baseCurrencyAmount, currencyRates).moneyAmountInCurrenciesMap

        //then
        assertTrue(currencyWalletMap.size == 1)

        assertTrue(currencyWalletMap.keys.contains(Currency.getInstance("EUR")))
        assertTrue(currencyWalletMap.get(Currency.getInstance("EUR")) == 100.0f)
    }
}