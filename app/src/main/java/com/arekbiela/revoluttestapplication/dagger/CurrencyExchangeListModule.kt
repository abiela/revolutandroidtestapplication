package com.arekbiela.revoluttestapplication.dagger

import com.arekbiela.revoluttestapplication.config.RevolutAppConfig
import com.arekbiela.revoluttestapplication.domain.calculator.CurrencyWalletCalculator
import com.arekbiela.revoluttestapplication.domain.converter.CurrencyWalletItemConverter
import com.arekbiela.revoluttestapplication.domain.converter.ListCurrencyWalletItemConverter
import com.arekbiela.revoluttestapplication.domain.interactors.CalculateCurrentAmountUseCase
import com.arekbiela.revoluttestapplication.domain.interactors.PollRevolutCurrencyRatesUseCase
import com.arekbiela.revoluttestapplication.domain.policy.ExponentialRxPollRetryPolicy
import com.arekbiela.revoluttestapplication.domain.policy.RxPollRetryPolicy
import com.arekbiela.revoluttestapplication.model.repository.CurrencyRatesRepository
import com.arekbiela.revoluttestapplication.presentation.model.CurrencyWalletItem
import dagger.Module
import dagger.Provides
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class CurrencyExchangeListModule {

    // DOMAIN ELEMENTS

    @Provides
    @Singleton
    fun providesRxRetryPolicy(revolutAppConfig: RevolutAppConfig): RxPollRetryPolicy<Long>
            = ExponentialRxPollRetryPolicy(revolutAppConfig.retryPolicyMaxCount)

    @Provides
    @Singleton
    fun providesCurrencyWalletCalculator(): CurrencyWalletCalculator
            = CurrencyWalletCalculator()

    @Provides
    @Singleton
    fun providesCurrencyWalletItemConverter(): ListCurrencyWalletItemConverter
            = ListCurrencyWalletItemConverter()

    //USE CASES

    @Provides
    @Singleton
    fun providesPollRevolutCurrencyRatesUseCase(repository: CurrencyRatesRepository,
                                                rxPollRetryPolicy: RxPollRetryPolicy<Long>,
                                                revolutAppConfig: RevolutAppConfig): PollRevolutCurrencyRatesUseCase
            = PollRevolutCurrencyRatesUseCase(repository, revolutAppConfig.pollCurrencyRatesTimeIntervalValue, revolutAppConfig.pollCurrencyRatesTimeIntervalTimeUnit, rxPollRetryPolicy)

    @Provides
    @Singleton
    fun providesCalculateCurrentAmountUseCase(pollRevolutCurrencyRatesUseCase: PollRevolutCurrencyRatesUseCase,
                                              currencyWalletCalculator: CurrencyWalletCalculator,
                                              converter: ListCurrencyWalletItemConverter,
                                              revolutAppConfig: RevolutAppConfig): CalculateCurrentAmountUseCase
            = CalculateCurrentAmountUseCase(pollRevolutCurrencyRatesUseCase, currencyWalletCalculator, converter, revolutAppConfig.defaultBaseCurrency, revolutAppConfig.defaultCurrencyAmount)
}