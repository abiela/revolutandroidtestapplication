package com.arekbiela.revoluttestapplication.dagger

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.arekbiela.revoluttestapplication.presentation.viewmodel.CurrencyListViewModel
import com.arekbiela.revoluttestapplication.presentation.viewmodel.CurrencyListViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CurrencyListViewModel::class)
    abstract fun bindCurrencyListViewModel(currencyListViewModel: CurrencyListViewModel): ViewModel

    @Binds
    abstract fun CurrencyListViewModelFactory(factory: CurrencyListViewModelFactory): ViewModelProvider.Factory
}