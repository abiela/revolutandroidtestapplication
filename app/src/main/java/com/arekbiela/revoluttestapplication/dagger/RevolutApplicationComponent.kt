package com.arekbiela.revoluttestapplication.dagger

import android.app.Application
import com.arekbiela.revoluttestapplication.RevolutApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component (modules = arrayOf(ApplicationModule::class, ActivityBuilderModule::class, CurrencyExchangeListModule::class, AndroidInjectionModule::class))
interface RevolutApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance fun application(application: Application): Builder
        fun build(): RevolutApplicationComponent
    }

    fun inject(application: RevolutApplication)
}