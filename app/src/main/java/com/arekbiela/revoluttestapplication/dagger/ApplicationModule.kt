package com.arekbiela.revoluttestapplication.dagger

import dagger.Module
import android.app.Application
import android.content.Context
import com.arekbiela.revoluttestapplication.config.RevolutAppConfig
import com.arekbiela.revoluttestapplication.model.api.RevolutCurrencyRatesApi
import com.arekbiela.revoluttestapplication.model.repository.CurrencyRatesRepository
import com.arekbiela.revoluttestapplication.model.repository.RevolutCurrencyRatesRepository
import javax.inject.Singleton
import dagger.Provides



@Module(includes = [ViewModelModule::class])
class ApplicationModule {

    @Provides
    @Singleton
    fun providesContext(application: Application): Context
            = application

    @Provides
    @Singleton
    fun providesRevolutAppConfig(): RevolutAppConfig
            = RevolutAppConfig.default()

    @Provides
    @Singleton
    fun providesRevolutCurrencyRatesApi(revolutAppConfig: RevolutAppConfig): RevolutCurrencyRatesApi
            = RevolutCurrencyRatesApi.build(revolutAppConfig.apiEndpoint)

    @Provides
    @Singleton
    fun providesCurrencyRatesRepository(revolutCurrencyRatesApi: RevolutCurrencyRatesApi): CurrencyRatesRepository
            = RevolutCurrencyRatesRepository(revolutCurrencyRatesApi)


}