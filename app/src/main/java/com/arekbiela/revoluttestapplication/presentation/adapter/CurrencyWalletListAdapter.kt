package com.arekbiela.revoluttestapplication.presentation.adapter

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.arekbiela.revoluttestapplication.*
import com.arekbiela.revoluttestapplication.presentation.listener.CurrencyAmountChangedItemEvent
import com.arekbiela.revoluttestapplication.presentation.listener.CurrencyBaseChangedItemEvent
import com.arekbiela.revoluttestapplication.presentation.listener.CurrentListItemEvent
import com.arekbiela.revoluttestapplication.presentation.model.CurrencyWalletItem
import com.arekbiela.revoluttestapplication.utils.CurrencyWalletItemDiffList
import com.squareup.picasso.Picasso
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_currency_value.view.*
import java.text.NumberFormat
import java.util.*

class CurrencyWalletListAdapter(private val context: Context,
                                var currencyWalletList: List<CurrencyWalletItem>) : RecyclerView.Adapter<CurrencyWalletListAdapter.CurrencyItemViewHolder>() {

    val clickEventSubject: PublishSubject<CurrentListItemEvent> = PublishSubject.create()
    private val numberFormat: NumberFormat = NumberFormat.getInstance(Locale.ROOT).also { it.maximumFractionDigits = 2 }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyItemViewHolder {
        val viewHolder = CurrencyItemViewHolder(LayoutInflater.from(context).inflate(R.layout.item_currency_value, parent, false))

        //Setting a text watcher for real-time currency amounts calculation
        val textWatcher = object: TextWatcher {
            override fun afterTextChanged(p0: Editable?)  {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(currentAmountText: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!currentAmountText.toString().isEmpty()) {
                    clickEventSubject.onNext(
                        CurrencyAmountChangedItemEvent(
                            currentAmountText.toString().toFloat()
                        )
                    )}
                }
        }

        //Setting item view on click listener
        viewHolder.itemView.setOnClickListener {
            if (viewHolder.adapterPosition != 0) {
                viewHolder.currencyAmountEditText.requestFocus()
            }
        }

        //Edit focus listener
        val edit = viewHolder.currencyAmountEditText
        edit.setOnFocusChangeListener { view, focused ->
                when(focused) {
                    true -> {
                        val selectedCurrencyItem = currencyWalletList[viewHolder.adapterPosition]
                        clickEventSubject.onNext(CurrencyBaseChangedItemEvent(selectedCurrencyItem.currency, selectedCurrencyItem.amount)
                        )
                        edit.addTextChangedListener(textWatcher)
                    }
                    false -> edit.removeTextChangedListener(textWatcher)
                }
            }
        return viewHolder
    }

    override fun onBindViewHolder(holder: CurrencyItemViewHolder, position: Int) {
        //Getting the currency item list
        val currencyItem = currencyWalletList[position]

        //Asynchronous image loading
        Picasso.get()
            .load(context.resources.getIdentifier(currencyItem.currency.currencyCode.toLowerCase() + "_flag", "drawable", context.applicationInfo.packageName))
            .into(holder.currencyFlagImage)

        //Text attributes loading
        holder.currencyCodeText?.text = currencyItem.currency.currencyCode
        holder.currencyNameText?.text = currencyItem.currency.displayName
        holder.currencyAmountEditText.let {
            it.setText(numberFormat.format(currencyItem.amount).apply { replace(",", ".") }, TextView.BufferType.EDITABLE)
            it.setSelection(it.text.length)
        }
    }

    override fun onBindViewHolder(holder: CurrencyItemViewHolder, position: Int, payloads: MutableList<Any>) {

        //Perform changes only to that elements that has been changed...
        if (!payloads.isEmpty()) {
            val diffBundle = payloads[0] as Bundle
            val currencyAmount = diffBundle.getFloat(CurrencyWalletItemDiffList.CURRENCY_AMOUNT)
            holder.currencyAmountEditText.let {
                if (!it.isFocused) {
                    it.setText(numberFormat.format(currencyAmount).apply { replace(",", ".") }, TextView.BufferType.EDITABLE)
                    it.setSelection(it.text.length)
                }
            }
        }

        //...or perform regular binding
        else {
            onBindViewHolder(holder, position)
        }
    }

    override fun getItemCount(): Int = currencyWalletList.size

    override fun getItemId(position: Int): Long = currencyWalletList[position].currency.hashCode().toLong()

    class CurrencyItemViewHolder(itemLayout: View) : RecyclerView.ViewHolder(itemLayout) {
        val currencyCodeText = itemLayout.currency_code
        val currencyNameText = itemLayout.currency_name
        val currencyAmountEditText = itemLayout.currency_amount
        val currencyFlagImage = itemLayout.currency_flag
    }
}