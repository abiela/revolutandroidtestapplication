package com.arekbiela.revoluttestapplication.presentation.model

import androidx.recyclerview.widget.DiffUtil

sealed class Response

class CurrencyValuesUpdate(val currencyWalletItemList: List<CurrencyWalletItem>, val diffResult: DiffUtil.DiffResult) : Response()
class CurrencyUpdateError(val message: String) : Response()
class CurrencyLoading(val message: String) : Response()