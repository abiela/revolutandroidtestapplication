package com.arekbiela.revoluttestapplication.presentation.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.arekbiela.revoluttestapplication.*
import com.arekbiela.revoluttestapplication.presentation.adapter.CurrencyWalletListAdapter
import com.arekbiela.revoluttestapplication.presentation.listener.CurrencyAmountChangedItemEvent
import com.arekbiela.revoluttestapplication.presentation.listener.CurrencyBaseChangedItemEvent
import com.arekbiela.revoluttestapplication.presentation.model.*
import com.arekbiela.revoluttestapplication.presentation.viewmodel.CurrencyListViewModel
import com.arekbiela.revoluttestapplication.presentation.viewmodel.CurrencyListViewModelFactory
import dagger.android.AndroidInjection
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var currencyListViewModelFactory: CurrencyListViewModelFactory

    private lateinit var viewModel: CurrencyListViewModel
    private lateinit var adapterEventsCompositeDisposable: CompositeDisposable

    private val currencyWalletListAdapter: CurrencyWalletListAdapter by lazy { CurrencyWalletListAdapter(this, emptyList()).also { it.setHasStableIds(true) }}


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        currency_value_list.adapter = currencyWalletListAdapter
        currency_value_list.layoutManager = LinearLayoutManager(this)
        currency_value_list.setHasFixedSize(true)

        viewModel = ViewModelProviders.of(this, currencyListViewModelFactory).get(CurrencyListViewModel::class.java)
        viewModel.getCurrencyUpdateResponse().observe(this, Observer<Response> {
                response ->
            when (response) {
            is CurrencyValuesUpdate -> showCurrencyUpdateResults(response)
            is CurrencyUpdateError -> showErrorScreenWith(response.message)
            is CurrencyLoading -> showLoadingScreenWith(response.message)
        }
        })

        retry_action_text.setOnClickListener { viewModel.triggerCalculationRetry() }
    }

    override fun onResume() {
        super.onResume()
        adapterEventsCompositeDisposable = CompositeDisposable().apply {
            add(currencyWalletListAdapter.clickEventSubject
                .subscribe {
                    when(it) {
                        is CurrencyAmountChangedItemEvent -> viewModel.changeBaseCurrencyAmount(it.currencyAmount.toString().toFloat())
                        is CurrencyBaseChangedItemEvent -> { viewModel.changeBaseCurrency(it.currency, it.amount) }
                    }
                })
        }
    }

    override fun onPause() {
        adapterEventsCompositeDisposable.dispose()
        super.onPause()
    }

    private fun showCurrencyUpdateResults(currencyValuesUpdate: CurrencyValuesUpdate) {
        if (!currency_value_list.isVisible) {
            progress_bar.visibility = View.GONE
            progress_text.visibility = View.GONE
            retry_action_text.visibility = View.GONE
            error_image.visibility = View.GONE
            error_text.visibility = View.GONE
            currency_value_list.visibility = View.VISIBLE
        }
        currencyWalletListAdapter.currencyWalletList = currencyValuesUpdate.currencyWalletItemList
        currencyValuesUpdate.diffResult.dispatchUpdatesTo(currencyWalletListAdapter)
    }

    private fun showLoadingScreenWith(message: String) {
        currency_value_list.visibility = View.GONE
        error_image.visibility = View.GONE
        error_text.visibility = View.GONE
        retry_action_text.visibility = View.GONE
        progress_bar.visibility = View.VISIBLE
        progress_text.text = message
        progress_text.visibility = View.VISIBLE
    }

    private fun showErrorScreenWith(message: String) {
        currency_value_list.visibility = View.GONE
        progress_bar.visibility = View.GONE
        progress_text.visibility = View.GONE
        retry_action_text.visibility = View.VISIBLE
        error_image.visibility = View.VISIBLE
        error_text.text = message
        error_text.visibility = View.VISIBLE
    }
}
