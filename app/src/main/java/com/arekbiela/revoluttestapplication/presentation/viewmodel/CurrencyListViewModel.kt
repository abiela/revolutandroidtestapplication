package com.arekbiela.revoluttestapplication.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DiffUtil
import com.arekbiela.revoluttestapplication.config.RevolutAppConfig
import com.arekbiela.revoluttestapplication.domain.interactors.CalculateCurrentAmountUseCase
import com.arekbiela.revoluttestapplication.presentation.model.*
import com.arekbiela.revoluttestapplication.utils.CurrencyWalletItemDiffList
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class CurrencyListViewModel @Inject constructor(private val calculateCurrentAmountUseCase: CalculateCurrentAmountUseCase,
                                                private val revolutAppConfig: RevolutAppConfig) : ViewModel() {

    private var compositeDisposable = CompositeDisposable()
    private val currencyUpdateResponse = MutableLiveData<Response>()
    private var lastCurrencyWalletItemList = emptyList<CurrencyWalletItem>()

    init {
        currencyUpdateResponse.value = CurrencyLoading(revolutAppConfig.loadingValuesMessage)
        subscribeToCalculationUseCase()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    private fun subscribeToCalculationUseCase() {
        compositeDisposable.add(
            calculateCurrentAmountUseCase
                .execute(revolutAppConfig.defaultBaseCurrency,revolutAppConfig.defaultCurrencyAmount)
                .map { currencyWalletItemList ->
                    val diffResult = DiffUtil.calculateDiff(CurrencyWalletItemDiffList(lastCurrencyWalletItemList, currencyWalletItemList))
                    lastCurrencyWalletItemList = currencyWalletItemList
                    Pair(currencyWalletItemList, diffResult)
                }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({pair -> currencyUpdateResponse.value = CurrencyValuesUpdate(pair.first, pair.second) }, { error -> currencyUpdateResponse.value = CurrencyUpdateError(error.message as String) })
        )
    }

    fun triggerCalculationRetry() {
        compositeDisposable.dispose()
        compositeDisposable = CompositeDisposable()
        currencyUpdateResponse.value = CurrencyLoading(revolutAppConfig.loadingValuesMessage)
        subscribeToCalculationUseCase()
    }

    fun changeBaseCurrencyAmount(baseCurrencyAmount: Float) = calculateCurrentAmountUseCase.changeAmount(baseCurrencyAmount)
    fun changeBaseCurrency(newBaseCurrency: Currency, currencyAmount: Float) = calculateCurrentAmountUseCase.changeBase(newBaseCurrency, currencyAmount)
    fun getCurrencyUpdateResponse() = currencyUpdateResponse
}