package com.arekbiela.revoluttestapplication.presentation.model

import java.util.*

data class CurrencyWallet(val moneyAmountInCurrenciesMap: Map<Currency, Float>)
data class CurrencyWalletItem(val currency: Currency, val amount: Float)