package com.arekbiela.revoluttestapplication.presentation.listener

import java.util.*

sealed class CurrentListItemEvent

data class CurrencyAmountChangedItemEvent(val currencyAmount: Float): CurrentListItemEvent()
data class CurrencyBaseChangedItemEvent(val currency: Currency, val amount: Float): CurrentListItemEvent()