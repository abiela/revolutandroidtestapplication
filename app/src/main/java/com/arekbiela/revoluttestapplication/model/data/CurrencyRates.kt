package com.arekbiela.revoluttestapplication.model.data

import com.google.gson.annotations.SerializedName
import java.util.*

data class CurrencyRates(@SerializedName("base")  val baseCurrency: Currency = Currency.getInstance("EUR"),
                         @SerializedName("rates") val ratesMap: Map<Currency, Float> = emptyMap())