package com.arekbiela.revoluttestapplication.model.repository

import com.arekbiela.revoluttestapplication.model.data.CurrencyRates
import io.reactivex.Single
import java.util.*

interface CurrencyRatesRepository {

    fun getSingleCurrencyRatesForBase(baseCurrency: Currency): Single<CurrencyRates>
}