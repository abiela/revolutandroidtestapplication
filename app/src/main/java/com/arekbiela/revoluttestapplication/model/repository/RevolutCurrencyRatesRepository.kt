package com.arekbiela.revoluttestapplication.model.repository

import com.arekbiela.revoluttestapplication.model.data.CurrencyRates
import com.arekbiela.revoluttestapplication.model.api.RevolutCurrencyRatesApi
import com.arekbiela.revoluttestapplication.model.exception.HttpCallFailureRequestException
import com.arekbiela.revoluttestapplication.model.exception.NetworkTimeoutRequestException
import com.arekbiela.revoluttestapplication.model.exception.ServerUnreachableRequestException
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.*
import java.util.concurrent.TimeUnit

class RevolutCurrencyRatesRepository(private val revolutCurrencyRatesApi: RevolutCurrencyRatesApi) :
    CurrencyRatesRepository {

    override fun getSingleCurrencyRatesForBase(baseCurrency: Currency): Single<CurrencyRates> =
        revolutCurrencyRatesApi
        .getCurrencyRatesForBase(baseCurrency)
        .onErrorResumeNext { errorThrowable ->
                when (errorThrowable) {
                    is SocketTimeoutException ->  Single.error(NetworkTimeoutRequestException("Oops! It seems that connectivity get lost! \n\n Please double check your internet connection."))
                    is UnknownHostException -> Single.error(ServerUnreachableRequestException("Oops! It seems that you're disconnected! \n\n Please double check your internet connection."))
                    is HttpException -> Single.error(HttpCallFailureRequestException("Oops! A request error happened! \n\n Please double check your internet connection."))
                    else -> Single.error(errorThrowable)
                }
            }
        .timeout(10, TimeUnit.SECONDS)
        .subscribeOn(Schedulers.computation())
}