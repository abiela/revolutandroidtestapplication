package com.arekbiela.revoluttestapplication.model.exception

import java.lang.RuntimeException

sealed class NetworkRequestException(message: String): RuntimeException(message)

class NetworkTimeoutRequestException(message: String): NetworkRequestException(message)
class ServerUnreachableRequestException(message: String): NetworkRequestException(message)
class HttpCallFailureRequestException(message: String): NetworkRequestException(message)
class MaxNetworkRequestRetriesExceeded(message: String): NetworkRequestException(message)