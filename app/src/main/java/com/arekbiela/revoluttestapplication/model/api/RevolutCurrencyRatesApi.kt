package com.arekbiela.revoluttestapplication.model.api

import com.arekbiela.revoluttestapplication.model.data.CurrencyRates
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

interface RevolutCurrencyRatesApi {

    @GET("latest")
    fun getCurrencyRatesForBase(@Query("base") baseCurrency: Currency): Single<CurrencyRates>

    companion object {
        fun build(endpoint: String): RevolutCurrencyRatesApi {
            val retrofit = Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
            return retrofit.create(RevolutCurrencyRatesApi::class.java)
        }
    }
}