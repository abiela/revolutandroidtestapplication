package com.arekbiela.revoluttestapplication.utils

import androidx.recyclerview.widget.DiffUtil
import com.arekbiela.revoluttestapplication.presentation.model.CurrencyWalletItem
import android.os.Bundle



class CurrencyWalletItemDiffList(private val oldList: List<CurrencyWalletItem>,
                                 private val newList: List<CurrencyWalletItem>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].currency.hashCode() == newList[newItemPosition].currency.hashCode()
    }

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].equals(newList[newItemPosition])
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val diff = Bundle()
        diff.putFloat(CurrencyWalletItemDiffList.CURRENCY_AMOUNT, newList[newItemPosition].amount)
        return diff
    }

    companion object {
        val CURRENCY_AMOUNT = "currency_amount"
    }
}