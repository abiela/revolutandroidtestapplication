package com.arekbiela.revoluttestapplication.domain.converter

import com.arekbiela.revoluttestapplication.presentation.model.CurrencyWallet

/**
 * Converts currency wallet into items applicable to UI.
 */

interface CurrencyWalletItemConverter<out T> {

    fun convertCurrencyWalletIntoItems(currencyWallet: CurrencyWallet): T
}