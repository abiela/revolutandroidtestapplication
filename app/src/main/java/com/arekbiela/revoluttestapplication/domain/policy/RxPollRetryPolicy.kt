package com.arekbiela.revoluttestapplication.domain.policy

import io.reactivex.Flowable

/**
 * RxJava-based polling retry policy.
 */

interface RxPollRetryPolicy<T> {

    fun getRetryPolicy(errorFlowable: Flowable<Throwable>): Flowable<T>
}