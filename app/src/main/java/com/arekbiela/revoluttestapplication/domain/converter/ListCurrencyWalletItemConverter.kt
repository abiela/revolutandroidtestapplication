package com.arekbiela.revoluttestapplication.domain.converter

import com.arekbiela.revoluttestapplication.presentation.model.CurrencyWallet
import com.arekbiela.revoluttestapplication.presentation.model.CurrencyWalletItem
import java.util.*

class ListCurrencyWalletItemConverter : CurrencyWalletItemConverter<List<CurrencyWalletItem>> {

    override fun convertCurrencyWalletIntoItems(currencyWallet: CurrencyWallet): List<CurrencyWalletItem> {
        val currencyWalletItemList = ArrayList<CurrencyWalletItem>()
        currencyWallet.moneyAmountInCurrenciesMap.entries.forEach { (currency, amount) -> currencyWalletItemList.add(
            CurrencyWalletItem(
                currency,
                amount
            )
        )}
        return currencyWalletItemList
    }
}