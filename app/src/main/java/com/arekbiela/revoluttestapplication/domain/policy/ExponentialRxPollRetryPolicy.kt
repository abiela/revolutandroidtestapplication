package com.arekbiela.revoluttestapplication.domain.policy

import com.arekbiela.revoluttestapplication.model.exception.HttpCallFailureRequestException
import com.arekbiela.revoluttestapplication.model.exception.MaxNetworkRequestRetriesExceeded
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import java.util.concurrent.TimeUnit

/**
 * Exponential RxJava-based polling retry policy.
 */

class ExponentialRxPollRetryPolicy(private val maxRetryCount: Int) :
    RxPollRetryPolicy<Long> {

    override fun getRetryPolicy(errorFlowable: Flowable<Throwable>): Flowable<Long> {
       return errorFlowable.zipWith(Flowable.range(1, maxRetryCount + 1), BiFunction<Throwable, Int, Int>  {error: Throwable, count: Int ->

           if (count > maxRetryCount) { throw MaxNetworkRequestRetriesExceeded(error.message as String) }

           if (error is HttpCallFailureRequestException) { count }

           else { throw error }
       } )
         .flatMap { retryCount: Int -> Flowable.timer(Math.pow(2.toDouble(), retryCount.toDouble()).toLong(), TimeUnit.SECONDS) }
    }
}