package com.arekbiela.revoluttestapplication.domain.interactors

import com.arekbiela.revoluttestapplication.presentation.model.CurrencyWalletItem
import com.arekbiela.revoluttestapplication.domain.converter.ListCurrencyWalletItemConverter
import com.arekbiela.revoluttestapplication.domain.calculator.CurrencyWalletCalculator
import com.arekbiela.revoluttestapplication.model.data.CurrencyRates
import com.arekbiela.revoluttestapplication.presentation.model.CurrencyWallet
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.*

/**
 * Calculates current currencies amount based on data from Revolut currency rates polling.
 */

class CalculateCurrentAmountUseCase(private val pollRevolutCurrencyRatesUseCase: PollRevolutCurrencyRatesUseCase,
                                    private val currencyWalletCalculator: CurrencyWalletCalculator,
                                    private val listCurrencyWalletItemConverter: ListCurrencyWalletItemConverter,
                                    private var currency: Currency = Currency.getInstance("EUR"),
                                    private var currencyAmount: Float = 0.0f) {

    private lateinit var amountSubject: PublishSubject<List<CurrencyWalletItem>>
    private var pollCurrencyRatesDisposable: CompositeDisposable = CompositeDisposable()
    private var lastCurrencyRates: CurrencyRates = CurrencyRates()

    fun execute(baseCurrency: Currency, baseCurrencyAmount: Float): Observable<List<CurrencyWalletItem>> {
        this.amountSubject = PublishSubject.create()
        this.currency = baseCurrency
        this.currencyAmount = baseCurrencyAmount
        subscribeToPollingUseCaseFor(baseCurrency)
        return amountSubject
    }

    private fun subscribeToPollingUseCaseFor(baseCurrency: Currency) {
        pollCurrencyRatesDisposable = CompositeDisposable()
        pollCurrencyRatesDisposable.add(
            pollRevolutCurrencyRatesUseCase
                .execute(baseCurrency)
                .saveLastCurrencyRates()
                .thenCalculateCurrencyWalletBasedOnCurrentCurrencyRatesAndAmount()
                .thenConvertWalletIntoItemList()
                .subscribeOn(Schedulers.computation())
                .subscribe({list -> amountSubject.onNext(list)}, {error -> amountSubject.onError(error)}))
    }

    fun changeAmount(newCurrencyAmount: Float) {
        currencyAmount = newCurrencyAmount
        val currencyWallet = currencyWalletCalculator.calculate(getBaseCurrencyAmount(), lastCurrencyRates)
        val currencyWalletItemList = listCurrencyWalletItemConverter.convertCurrencyWalletIntoItems(currencyWallet)
        amountSubject.onNext(currencyWalletItemList)
    }

    fun changeBase(currency: Currency, amount: Float) {
        pollCurrencyRatesDisposable.dispose()
        this.currency = currency
        this.currencyAmount = amount
        subscribeToPollingUseCaseFor(currency)
    }

    private fun getBaseCurrencyAmount() = currencyAmount

    private fun Observable<CurrencyRates>.saveLastCurrencyRates() = doOnNext { lastCurrencyRates = it }

    private fun Observable<CurrencyRates>.thenCalculateCurrencyWalletBasedOnCurrentCurrencyRatesAndAmount() =
        map { currencyRates ->  currencyWalletCalculator.calculate(getBaseCurrencyAmount(), currencyRates) }

    private fun Observable<CurrencyWallet>.thenConvertWalletIntoItemList() = map { currencyWallet -> listCurrencyWalletItemConverter.convertCurrencyWalletIntoItems(currencyWallet) }
}