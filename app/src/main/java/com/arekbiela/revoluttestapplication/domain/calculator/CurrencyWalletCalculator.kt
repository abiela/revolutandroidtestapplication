package com.arekbiela.revoluttestapplication.domain.calculator

import com.arekbiela.revoluttestapplication.presentation.model.CurrencyWallet
import com.arekbiela.revoluttestapplication.model.data.CurrencyRates
import java.util.*

/**
 * Calculates a currency wallet from currency rates and base currency amount.
 */

class CurrencyWalletCalculator {

    fun calculate(baseCurrencyAmount: Float, currencyRates: CurrencyRates): CurrencyWallet {
        val moneyAmountInCurrenciesMap = LinkedHashMap<Currency, Float>()
        moneyAmountInCurrenciesMap.put(currencyRates.baseCurrency, baseCurrencyAmount)
        currencyRates.ratesMap.entries.forEach { moneyAmountInCurrenciesMap.put(it.key, it.value * baseCurrencyAmount) }
        return CurrencyWallet(moneyAmountInCurrenciesMap)
    }
}