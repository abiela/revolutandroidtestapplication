package com.arekbiela.revoluttestapplication.domain.interactors

import com.arekbiela.revoluttestapplication.domain.policy.RxPollRetryPolicy
import com.arekbiela.revoluttestapplication.model.data.CurrencyRates
import com.arekbiela.revoluttestapplication.model.repository.CurrencyRatesRepository
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Polls a currency rates repository periodically, specified by polling interval and retried by polling retry policy.
 */

class PollRevolutCurrencyRatesUseCase(private val repository: CurrencyRatesRepository,
                                      private val pollingIntervalDelayAmount: Long,
                                      private val pollingIntervalDelayTimeUnit: TimeUnit,
                                      private val rxPollRetryPolicy: RxPollRetryPolicy<Long>
) {

    fun execute(baseCurrency: Currency): Observable<CurrencyRates> {
        return repository
            .getSingleCurrencyRatesForBase(baseCurrency)
            .retryWhen { rxPollRetryPolicy.getRetryPolicy(it) }
            .repeatWhen { it.delay(pollingIntervalDelayAmount, pollingIntervalDelayTimeUnit) }
            .toObservable()
            .subscribeOn(Schedulers.computation())
    }
}