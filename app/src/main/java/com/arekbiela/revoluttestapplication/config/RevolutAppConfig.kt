package com.arekbiela.revoluttestapplication.config

import java.util.*
import java.util.concurrent.TimeUnit

data class RevolutAppConfig(
    val apiEndpoint: String,
    val retryPolicyMaxCount: Int,
    val pollCurrencyRatesTimeIntervalValue: Long,
    val pollCurrencyRatesTimeIntervalTimeUnit: TimeUnit,
    val defaultBaseCurrency: Currency,
    val defaultCurrencyAmount: Float,
    val loadingValuesMessage: String) {

    companion object {
        fun default() = RevolutAppConfig(
            apiEndpoint = "https://revolut.duckdns.org/",
            retryPolicyMaxCount = 3,
            pollCurrencyRatesTimeIntervalValue = 1,
            pollCurrencyRatesTimeIntervalTimeUnit = TimeUnit.SECONDS,
            defaultBaseCurrency = Currency.getInstance("EUR"),
            defaultCurrencyAmount = 100.0f,
            loadingValuesMessage = "Currency table is loading..."
        )
    }
}


