# Android Revolut Test Application, March 2019

An Android application sample, built with a reply to test task: https://docs.google.com/document/d/13Ecs3hhgZJJLsugNUwZPUn_9gsqzwH80Bb-1CRbauTQ/edit

### Development prequisities

* LiveData
* ViewModel **2.0.0**
* Retrofit **2.5.0**
* Dagger **2.21**
* GSON Converter **2.3.0**
* RxJava 2 Adapter **1.0.0**
* RxJava **2.2.7**
* RxAndroid **2.0.1**
* Picasso **2.71828**
* Mockito **2.10.0**

### Architecture

![](./readme_images/arch.png)

* MVVM architecture
* RxJava stream-based use cases

### Key components

* **RevolutCurrencyRatesApi**
    * Making request for Revolut DNS currency rate API

* **RevolutCurrencyRatesRepository**
    * Repository pattern for dealing with remote data source (RevolutCurrencyRatesApi)
    * Local cache storage in the pattern is not necessary as there is no much value in storing last cached value (real-time only use case)
    * Yet, a possibility for expanding that repository with any LOCAL / REMOTE operations

* **PollRevolutCurrencyRatesUseCase**
    * Business logic responsible for periodic repository polling
    * Customizable repeat and retry policy
    * Returns a continuous stream of latest currency rates events

* **CalculateCurrentAmountUseCase**
    * Business logic responsible for real-time currency amount calculation
    * Based on latest currency rates and currency amount
    * Customizable currency item calculator and converter

* **CurrencyListViewModel**
    * Responsible for delivering business logic values to UI
    * Handling result updates states and trigger appropriate UI events
    * Works with respect for activity's lifecycle

### Error Handling
* Network related exceptions are translated into one of NetworkException sealed class options
* NetworkException classes as well as other exceptions are handled and displayed to the user

### Tests performed

Application tests were performed on the following mobile phones:
* Samsung Galaxy S4 (Android 5.0.1, API 21)
* HTC U11 Plus (Android 8.0.0, API 26)